package Exercitiul2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Interface extends JFrame {
    JTextField text1, text2,text3;
    JButton button;

    Interface() {

        setTitle("Suma caracterelor");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(600, 500);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);


        text1=new JTextField();
        text1.setBounds(200,40,200,40);

        text2=new JTextField();
        text2.setBounds(200,160,200,40);

        text3=new JTextField();
        text3.setBounds(200,280,200,40);
        text3.setEditable(false);

        button=new JButton("Suma");
        button.setBounds(260,400,80,40);


        button.addActionListener(new TratareButon());

        add(text2);
        add(text1);
        add(text3);
        add(button);
    }

    class TratareButon implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int nr1,nr2,nr3;
           String sir1,sir2,sir3;


           sir1=text1.getText();
           sir2=text2.getText();
           nr1=sir1.length();
           nr2=sir2.length();
           nr3=nr1+nr2;
           sir3=String.valueOf(nr3);
           text3.setText(sir3);


        }
    }


    public static void main(String[] args) {
        Interface I=new Interface();
    }

}



