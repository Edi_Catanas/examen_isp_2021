package Test;

public class Test {
        private int x = 5;
        private int y = 6;

        public Test(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "Test{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

